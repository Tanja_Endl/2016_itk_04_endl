﻿/*------------------------------------------------------------------------------------------------
 * Name: Vektor3D.cs
 * Autor: Tanja Endl
 * Datum: 25.05.2016
 * Beschreibung: erbt von Vektor2D
 *              enthält x,y,z Werte, eine Vektoraddition Methode und eine Längenberechnung Methode
 * -----------------------------------------------------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Übung_1_zu_Interfaces
{
    class Vektor3D : Vektor2D
    {
        public double x;
        public double y;
        public double z;

        public Vektor3D()
        {
        }
        public Vektor3D(double x, double y, double z)
            : base(x, y)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vektor3D Vektoraddition(Vektor3D vektor, Vektor3D vektor2)
        {
            vektor.x = vektor.x + vektor2.x;
            vektor.y = vektor.y + vektor2.y;
            vektor.z = vektor.y + vektor2.z;

            return vektor;
        }

        public double Laengenberechnung(Vektor3D vektor)
        {
            double laenge;
            laenge = Math.Sqrt(vektor.x * vektor.x + vektor.y * vektor.y + vektor.z * vektor.z);

            return laenge;
        }
    }
}
