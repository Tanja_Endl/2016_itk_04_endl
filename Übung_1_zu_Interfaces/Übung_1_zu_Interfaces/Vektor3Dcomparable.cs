﻿/*------------------------------------------------------------------------------------------------
 * Name: Vektor3D.cs
 * Autor: Tanja Endl
 * Datum: 25.05.2016
 * Beschreibung: erbt von Vektor3D, IComparable , ICloneable
 *              enthält x,y,z Werte, eine CompareTo Methode und eine Clone Methode
 * -----------------------------------------------------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Übung_1_zu_Interfaces
{
    class Vektor3Dcomparable : Vektor3D, IComparable , ICloneable
    {
        
        public Vektor3Dcomparable()
        {
        }
        public Vektor3Dcomparable(
            double x, double y, double z)
            : base(x, y, z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
     
        public int CompareTo(object o)
        {

            if (o == null) return 1;

            Vektor3Dcomparable v3 = o as Vektor3Dcomparable;
            if (v3 != null)
                return this.Laengenberechnung(this).CompareTo(v3.Laengenberechnung(v3));
            else
                throw new ArgumentException("Vektor hat keine Länge");

        }

        public object Clone()
        {
            return new Vektor3Dcomparable(this.x, this.y, this.z);
        }
    }
}
