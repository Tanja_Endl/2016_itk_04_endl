﻿/*-----------------------------------
 * Name: Program.cs
 * Autor: Tanja Endl
 * Datum: 25.05.2016
 * Beschreibung: erzeugt Vektoren
 * ---------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Übung_1_zu_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {

            Vektor2D v = new Vektor2D();
            Console.WriteLine("Vektor 1 x:");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Vektor 1 y:");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Vektor 2 x:");
            double c = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Vektor 2 y:");
            double d = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Vektor 3 x:");
            double e = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Vektor 3 y:");
            double f = Convert.ToDouble(Console.ReadLine());
            Vektor2D v1 = new Vektor2D(a, b);
            Vektor2D v2 = new Vektor2D(c,d);
            Vektor2D v3 = new Vektor2D(e, f);
            Vektor2D v12 = new Vektor2D();
            Vektor2D vGesamt = new Vektor2D();
            Vektor3Dcomparable v4 = new Vektor3Dcomparable(4, 2, 3);
            Vektor3Dcomparable v5 = new Vektor3Dcomparable(1, 4, 5);
            Vektor3Dcomparable v6 = new Vektor3Dcomparable(9, 6, 7);
            Vektor3Dcomparable v7 = new Vektor3Dcomparable(6, 4, 5);
            Vektor3Dcomparable v8 = new Vektor3Dcomparable(9, 1, 2);
            List<Vektor3Dcomparable> v3li = new List<Vektor3Dcomparable>();
            v3li.Add(v4);
            v3li.Add(v5);
            v3li.Add(v6);
            v3li.Add(v7);
            v3li.Add(v8);


            //Nummer 1
            Console.WriteLine("Länge Vektor1: \n" + v.Laengenberechnung(v1));
            Console.WriteLine("Länge Vektor2: \n" + v.Laengenberechnung(v2));
            Console.WriteLine("Länge Vektor3: \n" + v.Laengenberechnung(v3));

            v12 = v.Vektoraddition(v1, v2);
            vGesamt = v.Vektoraddition(v12, v3);

            Console.WriteLine("Länge alle Vektoren: \n" + v.Laengenberechnung(vGesamt));


            //Nummer 3
            v3li.Sort();
            Console.WriteLine("Vergleich: ");
            foreach (Vektor3D v3c in v3li)
            {
                Console.WriteLine(v3c);
            }
            //Nummer 4) a)
            List<Vektor3Dcomparable> v3A = new List<Vektor3Dcomparable>();
            Vektor3Dcomparable vA = new Vektor3Dcomparable(4, 2, 3);
            v3A.Add(vA);
            v3A.Add(vA);
            v3A.Add(vA);

            Vektor3Dcomparable vI = new Vektor3Dcomparable();
            Vektor3D vAGesamt = new Vektor3D();
            Vektor3D vl = new Vektor3D();
            for (int i = 1; i < v3A.Count; i++)
            {
                vAGesamt = vI.Vektoraddition((Vektor3D)v3A[i - 1], (Vektor3D)v3A[i]);
            }
            Console.WriteLine("Länge Vektorliste Nummer 4 A: \n" + vl.Laengenberechnung(vAGesamt));


            //Nummer 4) b)
            List<Vektor3Dcomparable> v3B = new List<Vektor3Dcomparable>();
            Vektor3Dcomparable vB = new Vektor3Dcomparable(4, 2, 3);
            v3B.Add((Vektor3Dcomparable)vB.Clone());
            v3B.Add((Vektor3Dcomparable)vB.Clone());
            v3B.Add((Vektor3Dcomparable)vB.Clone());

            Vektor3Dcomparable vO = new Vektor3Dcomparable();
            Vektor3D vOGesamt = new Vektor3D();
            Vektor3D vF = new Vektor3D();
            for (int i = 1; i < v3B.Count; i++)
            {
                vOGesamt = vO.Vektoraddition((Vektor3D)v3B[i - 1], (Vektor3D)v3B[i]);
            }
            Console.WriteLine("Länge Vektorliste Nummer 4 B: \n" + vl.Laengenberechnung(vOGesamt));

            Console.ReadLine();
        }
    }
}
