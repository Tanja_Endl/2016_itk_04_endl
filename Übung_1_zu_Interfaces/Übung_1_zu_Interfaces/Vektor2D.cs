﻿/*------------------------------------------------------------------------------------------------
 * Name: Vektor2D.cs
 * Autor: Tanja Endl
 * Datum: 25.05.2016
 * Beschreibung: enthält x,y Werte, eine Vektoraddition Methode und eine Längenberechnung Methode
 * -----------------------------------------------------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Übung_1_zu_Interfaces
{
    class Vektor2D
    {
        public double x;
        public double y;

        public Vektor2D()
        {
        }
        public Vektor2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public Vektor2D Vektoraddition(Vektor2D vektor,Vektor2D vektor2)
        {
            vektor.x =vektor.x + vektor2.x;
            vektor.y = vektor.y + vektor2.y;

            return vektor;
        }

        public double Laengenberechnung(Vektor2D vektor)
        {
            double laenge;

            laenge = Math.Sqrt(vektor.x*vektor.x+vektor.y*vektor.y);

            return laenge;
        }

        public override string ToString()
        {
            return Convert.ToString(Laengenberechnung(this));
        }
    }
}
