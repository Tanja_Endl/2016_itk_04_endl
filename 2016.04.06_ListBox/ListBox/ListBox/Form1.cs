﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ListBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Datei da = (Datei)listBox1.SelectedItem;
            MessageBox.Show(da.Time);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            DialogResult d = folderBrowserDialog.ShowDialog();


            if (d == DialogResult.OK)
            {
                string[] file = Directory.GetFiles(folderBrowserDialog.SelectedPath);
                List<Datei> dli = new List<Datei>();

                foreach (string st in file)
                {
                    string time = Convert.ToString(Directory.GetLastAccessTime(st));
                    string name = Path.GetFileName(st);

                    dli.Add(new Datei(time, name));
                }
                foreach (Datei s in dli)
                {
                    listBox1.Items.Add(s);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

        }

    }
}
