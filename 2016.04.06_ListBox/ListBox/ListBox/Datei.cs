﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListBox
{
    class Datei
    {
        public Datei(string time, string name)
        {
            Time = time;
            Name = name;
        }
        public string Time { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}
