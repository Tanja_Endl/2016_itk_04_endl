﻿/*---------------------------------------------------------
 * Name: Kategorie.cs                                      |
 * Autor: Tanja Endl                                       |
 * Datum: 04.05.2016                                       |
 * Beschreibung: alle Eigenschaften der Tabelle Kategorie  |
 * --------------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Übung_3_zu_DB_Zugriff
{
    public class Kategorie
    {
        int id;
        string kbez;

        public int ID { get; set; }
        public string Kbez { get; set; }

        public Kategorie()
        {
            ID = id;
            Kbez = kbez;
        }

        public Kategorie(int id, string kbez)
        {
            ID = id;
            Kbez = kbez;
        }

        public override string ToString()
        {
            return Kbez;
        }

    }
}
