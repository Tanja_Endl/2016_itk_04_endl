﻿/*-----------------------------------------------------------------
 * Name: Artikel.cs                                                |
 * Autor: Tanja Endl                                               |
 * Datum: 27.04.2016                                               |
 * Beschreibung: alle eigenschaften der Tabelle Artikel            |
 * -----------------------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace Übung_3_zu_DB_Zugriff
{
    class Artikel
    {
        int id;
        string ean;
        string bez;
        int lager;
        DateTime sortiment;
        double vkp;
        Kategorie kat;
        public bool isDirty = false;
        public bool isNew = false;
        public int ID
        {
            get { return id; }
            set
            {
                if (value != id)
                { isDirty = true; }
                id = value;
            }
        }
        public string Ean
        {
            get { return ean; }
            set
            {
                if (value != ean)
                { isDirty = true; }
                ean = value;
            }
        }
        public string Bez
        {
            get { return bez; }
            set
            {
                if (value != bez)
                { isDirty = true; }
                bez = value;
            }
        }
        public int Lager
        {
            get { return lager; }
            set
            {
                if (value != lager)
                { isDirty = true; }
                lager = value;
            }
        }
        public DateTime Sortiment
        {
            get { return sortiment; }
            set
            {
                if (value != sortiment)
                { isDirty = true; }
                sortiment = value;
            }
        }
        public double Vkp
        {
            get { return vkp; }
            set
            {
                if (value != vkp)
                { isDirty = true; }
                vkp = value;
            }
        }
        public Kategorie Kat
        {
            get { return kat; }
            set
            {
                if (value != kat)
                { isDirty = true; }
                kat = value;
            }
        }

        public Artikel(int id, string ean, string bez, int lager, DateTime sortiment, double vkp, Kategorie kat)
        {
            this.id = id;
            this.ean = ean;
            this.bez = bez;
            this.lager = lager;
            this.sortiment = sortiment;
            this.vkp = vkp;
            this.kat = kat;
        }
        public override string ToString()
        {
            string dreißig = "";
            if (bez.Length >= 30)
            {
                for (int i = 0; i < 31; i++)
                {
                    dreißig += Bez[i];
                }

                return dreißig;

            }
            else
            {
                return bez;
            }
        }
    }
}
