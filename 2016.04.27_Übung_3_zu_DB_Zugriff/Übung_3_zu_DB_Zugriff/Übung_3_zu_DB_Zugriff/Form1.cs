﻿/*------------------------------------------------------------------
 * Name: Form1.cs                                                   |
 * Autor: Tanja Endl                                                |
 * Datum: 27.04.2016                                                |
 * Beschreibung: Nach Klick eines Buttons wird das Formular befüllt |
 * -----------------------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Übung_3_zu_DB_Zugriff
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=artikelDB.mdb";
            try
            {
                con = new OleDbConnection(conStr);
                con.Open();
                fillListBox();
            }
            catch (OleDbException oledb)
            {
                MessageBox.Show(oledb + "Etwas ist mit der Verbindung schiefgelaufen!");
            }

            this.comboBox_Kategorie.Items.AddRange(KatAuslesen().ToArray());

        }

        OleDbConnection con;
        Artikel ac = null;
        bool newFlag;

        public void fillListBox()
        {
            listBox.Items.Clear();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from Artikel order by Bezeichnung asc";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string ean = reader.GetString(1);
                string bez = reader.GetString(2);
                int lager = reader.GetInt32(3);
                DateTime sortiment = reader.GetDateTime(4);
                double vkp = reader.GetDouble(5);

                int kid;
                if (Convert.IsDBNull(reader.GetValue(6)))
                    kid = 1;
                else
                    kid = reader.GetInt32(6);

                Kategorie kat = new Kategorie();
                kat.ID = kid;

                Artikel artikel = new Artikel(id, ean, bez, lager, sortiment, vkp, kat);
                listBox.Items.Add(artikel);
            }
            reader.Close();
        }

        public void Neuanlage()
        {
            Artikel a = (Artikel)listBox.SelectedItem;

            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            Kategorie kat = (Kategorie)comboBox_Kategorie.SelectedItem;
            cmd.CommandText = "insert into Artikel (EAN, Bezeichnung, Lager, ImSortiment, VKP, Kategorie) values ('" + BoxEAN.Text + "','" + BoxBezeichnung.Text + "'," + Convert.ToInt32(BoxLagerstand.Text) + ",'" + dateTimePicker1.Value + "'," + Convert.ToDouble(BoxPreis.Text) + ", " + kat.ID + ");";
            cmd.ExecuteNonQuery();
        }

        public List<Kategorie> KatAuslesen()
        {
            List<Kategorie> output = new List<Kategorie>();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select ID,Bez from Kategorie;";
            OleDbDataReader reader = cmd.ExecuteReader();
            int kId = 0;
            string kBez = "";
            while (reader.Read())
            {
                kId = reader.GetInt32(0);
                kBez = reader.GetString(1);
                Kategorie kat = new Kategorie(kId, kBez);
                output.Add(kat);
            }
            reader.Close();

            return output;
        }

        public void FormularBefuellen()
        {
            Artikel wahl = (Artikel)listBox.SelectedItem;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select EAN,Bezeichnung,Lager,ImSortiment, VKP, Kategorie from Artikel WHERE ID = " + wahl.ID + ";";
            OleDbDataReader reader = cmd.ExecuteReader();

            string EAN;
            string bez;
            int lager;
            DateTime sort;
            double vkp;
            int kid;
            comboBox_Kategorie.SelectedIndex = 2;
            while (reader.Read())
            {
                if (reader.GetValue(0) != System.DBNull.Value)
                {
                    EAN = reader.GetString(0);
                }
                else
                {
                    EAN = "-";
                }

                if (reader.GetValue(1) != System.DBNull.Value)
                {
                    bez = reader.GetString(1);
                }
                else
                {
                    bez = "-";
                }
                if (reader.GetValue(2) != System.DBNull.Value)
                {
                    lager = reader.GetInt32(2);
                }
                else
                {
                    lager = 0;
                }
                if (reader.GetValue(3) != System.DBNull.Value)
                {
                    sort = reader.GetDateTime(3);
                }
                else
                {
                    sort = DateTime.Now;
                }
                if (reader.GetValue(4) != System.DBNull.Value)
                {
                    vkp = reader.GetDouble(4);
                }
                else
                {
                    vkp = 0;
                }
                if (reader.GetValue(5) != System.DBNull.Value)
                {
                    kid = reader.GetInt32(5);
                }
                else
                {
                    kid = 3;
                }

                BoxEAN.Text = EAN;
                BoxBezeichnung.Text = bez;
                BoxLagerstand.Text = Convert.ToString(lager);
                dateTimePicker1.Value = sort;
                BoxPreis.Text = Convert.ToString(vkp);

                foreach (Kategorie k in this.comboBox_Kategorie.Items)
                {
                    if (k.ID == kid)
                        comboBox_Kategorie.SelectedItem = k;
                }

                ac = new Artikel(wahl.ID, EAN, bez, lager, sort, vkp, comboBox_Kategorie.SelectedItem as Kategorie);
            }
            reader.Close();

        }

        public void Aenderung()
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;

            Artikel a = (Artikel)listBox.SelectedItem;
            Kategorie kat = (Kategorie)comboBox_Kategorie.SelectedItem;

            cmd.CommandText = "update artikel set EAN= '" + BoxEAN.Text + "', Bezeichnung='" + BoxBezeichnung.Text + "',VKP = '" + BoxPreis.Text + "',ImSortiment = '" + dateTimePicker1.Value + "' ,Lager = '" + BoxLagerstand.Text + "', Kategorie =" + kat.ID + " where Id =" + a.ID;

            try
            {
                MessageBox.Show(cmd.ExecuteNonQuery() + " Row affected.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "Hallo");
            }

        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormularBefuellen();
        }

        private void Neu_Click(object sender, EventArgs e)
        {
            newFlag = true;
            BoxEAN.Clear();
            BoxBezeichnung.Clear();
            BoxLagerstand.Clear();
            BoxPreis.Clear();
            BoxBezeichnung.Clear();
            comboBox_Kategorie.SelectedIndex = 2;
        }

        private void Speichern_Click(object sender, EventArgs e)
        {
            int lager;
            double vkp;
            ac.Ean = BoxEAN.Text;
            ac.Bez = BoxBezeichnung.Text;
            ac.Kat = (Kategorie)comboBox_Kategorie.SelectedItem;
            bool falschewerte = false;
            if (!(Double.TryParse(BoxPreis.Text, out vkp)))
            {
                MessageBox.Show("Fehler bei Preis!(Buchstabe)");
                falschewerte = true;
            }
            else
            {
                if (vkp < 0)
                {
                    falschewerte = true;
                }
                else
                    ac.Vkp = Convert.ToDouble(BoxPreis.Text);
            }

            if (!(Int32.TryParse(BoxLagerstand.Text, out lager)))
            {
                MessageBox.Show("Fehler bei Lagerstand!(Buchstabe)");
                falschewerte = true;
            }
            else
                ac.Lager = Convert.ToInt32(BoxLagerstand.Text);
            if (BoxEAN.Text.Length > 13)
            {
                falschewerte = true;
            }

            if (falschewerte == false)
            {
                if (newFlag == true)
                {
                    Neuanlage();
                }
                else
                {
                    if (ac.isDirty)
                    {
                        Aenderung();
                    }
                }
            }

            fillListBox();
        }

        private void Loeschen_Click(object sender, EventArgs e)
        {
            Artikel a = (Artikel)listBox.SelectedItem;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "delete * from Artikel where ID=" + a.ID + ";";
            cmd.ExecuteNonQuery();
            fillListBox();
            BoxEAN.Clear();
            BoxBezeichnung.Clear();
            BoxLagerstand.Clear();
            BoxPreis.Clear();
            BoxBezeichnung.Clear();
            comboBox_Kategorie.SelectedItem = -1;

        }

        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            con.Close();
        }

        private void comboBox_Kategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            KatAuslesen();
        }

    }
}
