﻿namespace Übung_3_zu_DB_Zugriff
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.listBox = new System.Windows.Forms.ListBox();
            this.BoxEAN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Neu = new System.Windows.Forms.Button();
            this.Speichern = new System.Windows.Forms.Button();
            this.Loeschen = new System.Windows.Forms.Button();
            this.BoxBezeichnung = new System.Windows.Forms.TextBox();
            this.BoxPreis = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.BoxLagerstand = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_Kategorie = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Artikelverwaltung";
            // 
            // listBox
            // 
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(21, 49);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(177, 264);
            this.listBox.TabIndex = 1;
            this.listBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
            // 
            // BoxEAN
            // 
            this.BoxEAN.Location = new System.Drawing.Point(412, 42);
            this.BoxEAN.Name = "BoxEAN";
            this.BoxEAN.Size = new System.Drawing.Size(197, 20);
            this.BoxEAN.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(303, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "EAN:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(303, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Bezeichnung:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(303, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Verkaufspreis:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(303, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "im Sortiment ab:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(303, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Lagerstand:";
            // 
            // Neu
            // 
            this.Neu.Location = new System.Drawing.Point(291, 321);
            this.Neu.Name = "Neu";
            this.Neu.Size = new System.Drawing.Size(94, 23);
            this.Neu.TabIndex = 8;
            this.Neu.Text = "Neuanlage";
            this.Neu.UseVisualStyleBackColor = true;
            this.Neu.Click += new System.EventHandler(this.Neu_Click);
            // 
            // Speichern
            // 
            this.Speichern.Location = new System.Drawing.Point(412, 321);
            this.Speichern.Name = "Speichern";
            this.Speichern.Size = new System.Drawing.Size(94, 23);
            this.Speichern.TabIndex = 9;
            this.Speichern.Text = "Speichern";
            this.Speichern.UseVisualStyleBackColor = true;
            this.Speichern.Click += new System.EventHandler(this.Speichern_Click);
            // 
            // Loeschen
            // 
            this.Loeschen.Location = new System.Drawing.Point(515, 321);
            this.Loeschen.Name = "Loeschen";
            this.Loeschen.Size = new System.Drawing.Size(94, 23);
            this.Loeschen.TabIndex = 10;
            this.Loeschen.Text = "Löschen";
            this.Loeschen.UseVisualStyleBackColor = true;
            this.Loeschen.Click += new System.EventHandler(this.Loeschen_Click);
            // 
            // BoxBezeichnung
            // 
            this.BoxBezeichnung.Location = new System.Drawing.Point(412, 89);
            this.BoxBezeichnung.Multiline = true;
            this.BoxBezeichnung.Name = "BoxBezeichnung";
            this.BoxBezeichnung.Size = new System.Drawing.Size(197, 81);
            this.BoxBezeichnung.TabIndex = 11;
            // 
            // BoxPreis
            // 
            this.BoxPreis.Location = new System.Drawing.Point(412, 197);
            this.BoxPreis.Name = "BoxPreis";
            this.BoxPreis.Size = new System.Drawing.Size(110, 20);
            this.BoxPreis.TabIndex = 12;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(412, 229);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(184, 20);
            this.dateTimePicker1.TabIndex = 13;
            // 
            // BoxLagerstand
            // 
            this.BoxLagerstand.Location = new System.Drawing.Point(412, 255);
            this.BoxLagerstand.Name = "BoxLagerstand";
            this.BoxLagerstand.Size = new System.Drawing.Size(110, 20);
            this.BoxLagerstand.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(311, 284);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Kategorie:";
            // 
            // comboBox_Kategorie
            // 
            this.comboBox_Kategorie.FormattingEnabled = true;
            this.comboBox_Kategorie.Location = new System.Drawing.Point(412, 281);
            this.comboBox_Kategorie.Name = "comboBox_Kategorie";
            this.comboBox_Kategorie.Size = new System.Drawing.Size(184, 21);
            this.comboBox_Kategorie.TabIndex = 16;
            this.comboBox_Kategorie.SelectedIndexChanged += new System.EventHandler(this.comboBox_Kategorie_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 370);
            this.Controls.Add(this.comboBox_Kategorie);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.BoxLagerstand);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.BoxPreis);
            this.Controls.Add(this.BoxBezeichnung);
            this.Controls.Add(this.Loeschen);
            this.Controls.Add(this.Speichern);
            this.Controls.Add(this.Neu);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BoxEAN);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Ersatzteillager Wien";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox;
        private System.Windows.Forms.TextBox BoxEAN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Neu;
        private System.Windows.Forms.Button Speichern;
        private System.Windows.Forms.Button Loeschen;
        private System.Windows.Forms.TextBox BoxBezeichnung;
        private System.Windows.Forms.TextBox BoxPreis;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox BoxLagerstand;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_Kategorie;
    }
}

