﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerlarm
{
    class Schule
    {
        public Schule()
        {
 
        }
        public delegate void DelTyp(string typ);
        public event DelTyp fire; 

        public int GetPlatzNummer(Klasse k)
        {
            switch (k.Name)
            {
                case "3BHITT":
                       return 1;


                case "2BHITT":
                    return 2;
                    

                default:
                    return 5;
                    

            }
        }

        public void StartFeueralarm(string typ)
        {
            fire(typ);
        }

        
    }
}
