﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerlarm
{
    class Program
    {
        static void Main(string[] args)
        {
            Schule schule = new Schule();
            Klasse klasse = new Klasse("3BHITT");
            Schueler s1 = new Schueler("Ferdinand", schule, klasse);
            Schueler s2 = new Schueler("Franz", schule, klasse);
            Schueler s3 = new Schueler("Maria", schule, klasse);
            klasse = new Klasse("2BHITT");
            s1 = new Schueler("Fritz", schule, klasse);
            s2 = new Schueler("Martha", schule, klasse);
            schule.StartFeueralarm("Probealarm");
        }
    }
}
