﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerlarm
{
    class Schueler
    {
        string name;
        Klasse klasse;
        Schule schule;

        public Schueler(string n, Schule s, Klasse k)
        {
            name = n;
            schule = s;
            klasse = k;
            s.fire += s_fire;
        }

        void s_fire(string typ)
        {
            
            Console.WriteLine(typ+"Schüler: "+name+" begibt sich in den Hof "+schule.GetPlatzNummer(klasse));
        }
    }
}
