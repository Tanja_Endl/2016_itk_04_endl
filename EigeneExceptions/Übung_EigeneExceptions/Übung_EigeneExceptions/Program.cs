﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Übung_EigeneExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int>();
            double av=0;
            try
            {
                Console.WriteLine("Der Mittelwert ist "+average(list));
                Console.WriteLine(averageTryParse(list,out av));
            }
            catch (Exception ex)
            {
               
                ex= MyException();
                ex.Data.Add("InfoM", "Information:");                
                Console.WriteLine(ex.Message);
                
                //Console.WriteLine("Kein Element in der Liste!");
                //list.Add(3);
                //list.Add(2);
                //list.Add(8);
                //list.Add(45);
                //list.Add(60);
                //list.Add(1);
                //Console.WriteLine("Es werden automatisch die Werte " + list[0] + ", " + list[1] + ", " + list[2] + ", " + list[3] + ", " + list[4] + "," + list[5] + "  hinzugefügt!");
                //Console.WriteLine("Der Mittelwert ist " + average(list));  
            }
           
        }

        static Exception MyException()
        {
            
            Exception MyFirstException = new Exception();
            
            return MyFirstException;

        }

        static public double average (List<int> list)
        {
                int gesamtwert=0;
                double mittelwert=0;
                int anzahl = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    gesamtwert = gesamtwert + list[i];
                    anzahl++;          
                }
                mittelwert = gesamtwert / anzahl;
                
                return mittelwert;
        }

        static public bool averageTryParse(List<int> l, out double av)
        {
            int gesamtwert = 0;
            int anzahl = 0;

            for (int i = 0; i < l.Count; i++)
            {
                gesamtwert = gesamtwert + l[i];
                anzahl++;
            }
            av = gesamtwert / anzahl;
            
            if (av==null)
            {
                return false;
            }
            else
            {
               return true;
            }
            
        }
    }
}
