﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Calculator
{
    class Vector
    {
        public int x;
        public int y;

        public Vector(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return x+" "+y;
        }



        public void Add(Vector v2)
        {
            this.x += v2.x;
            this.y += v2.y;
        }
    }
}
