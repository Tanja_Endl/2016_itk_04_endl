﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.xVector1 = new System.Windows.Forms.TextBox();
            this.xVector2 = new System.Windows.Forms.TextBox();
            this.yVector2 = new System.Windows.Forms.TextBox();
            this.yVector1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ergebnis = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vektor 1:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(190, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y-Wert";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Vektor 2:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(117, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "X-Wert";
            // 
            // xVector1
            // 
            this.xVector1.Location = new System.Drawing.Point(101, 56);
            this.xVector1.Name = "xVector1";
            this.xVector1.Size = new System.Drawing.Size(56, 20);
            this.xVector1.TabIndex = 5;
            // 
            // xVector2
            // 
            this.xVector2.Location = new System.Drawing.Point(101, 110);
            this.xVector2.Name = "xVector2";
            this.xVector2.Size = new System.Drawing.Size(56, 20);
            this.xVector2.TabIndex = 6;
            // 
            // yVector2
            // 
            this.yVector2.Location = new System.Drawing.Point(174, 110);
            this.yVector2.Name = "yVector2";
            this.yVector2.Size = new System.Drawing.Size(56, 20);
            this.yVector2.TabIndex = 7;
            // 
            // yVector1
            // 
            this.yVector1.Location = new System.Drawing.Point(174, 56);
            this.yVector1.Name = "yVector1";
            this.yVector1.Size = new System.Drawing.Size(56, 20);
            this.yVector1.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(101, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Berechnen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ergebnis
            // 
            this.ergebnis.AutoSize = true;
            this.ergebnis.Location = new System.Drawing.Point(92, 214);
            this.ergebnis.Name = "ergebnis";
            this.ergebnis.Size = new System.Drawing.Size(84, 13);
            this.ergebnis.TabIndex = 10;
            this.ergebnis.Text = "Nicht Berechnet";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ergebnis);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.yVector1);
            this.Controls.Add(this.yVector2);
            this.Controls.Add(this.xVector2);
            this.Controls.Add(this.xVector1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox xVector1;
        private System.Windows.Forms.TextBox xVector2;
        private System.Windows.Forms.TextBox yVector2;
        private System.Windows.Forms.TextBox yVector1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label ergebnis;
    }
}