﻿namespace VokTrainer_1
{
    partial class einstellungen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(einstellungen));
            this.Speichern = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.anzahlv = new System.Windows.Forms.NumericUpDown();
            this.modusv = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.prozentEins = new System.Windows.Forms.TextBox();
            this.prozentZwei = new System.Windows.Forms.TextBox();
            this.prozentDrei = new System.Windows.Forms.TextBox();
            this.prozentVier = new System.Windows.Forms.TextBox();
            this.prozentFünf = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.eins = new System.Windows.Forms.TextBox();
            this.zwei = new System.Windows.Forms.TextBox();
            this.drei = new System.Windows.Forms.TextBox();
            this.vier = new System.Windows.Forms.TextBox();
            this.fuenf = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.anzahlv)).BeginInit();
            this.SuspendLayout();
            // 
            // Speichern
            // 
            this.Speichern.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Speichern.Location = new System.Drawing.Point(58, 270);
            this.Speichern.Name = "Speichern";
            this.Speichern.Size = new System.Drawing.Size(75, 23);
            this.Speichern.TabIndex = 0;
            this.Speichern.Text = "Speichern";
            this.Speichern.UseVisualStyleBackColor = true;
            this.Speichern.Click += new System.EventHandler(this.Speichern_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(166, 270);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Abbrechen";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Anzahl der Vokabel:";
            // 
            // anzahlv
            // 
            this.anzahlv.Location = new System.Drawing.Point(148, 7);
            this.anzahlv.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.anzahlv.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.anzahlv.Name = "anzahlv";
            this.anzahlv.Size = new System.Drawing.Size(92, 20);
            this.anzahlv.TabIndex = 3;
            this.anzahlv.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.anzahlv.ValueChanged += new System.EventHandler(this.anzahlv_ValueChanged);
            // 
            // modusv
            // 
            this.modusv.FormattingEnabled = true;
            this.modusv.Items.AddRange(new object[] {
            "Englisch -> Deutsch",
            "Deutsch -> Englisch"});
            this.modusv.Location = new System.Drawing.Point(147, 42);
            this.modusv.Name = "modusv";
            this.modusv.Size = new System.Drawing.Size(93, 21);
            this.modusv.TabIndex = 4;
            this.modusv.SelectedIndexChanged += new System.EventHandler(this.modusv_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Modus:";
            // 
            // prozentEins
            // 
            this.prozentEins.Location = new System.Drawing.Point(147, 110);
            this.prozentEins.Name = "prozentEins";
            this.prozentEins.Size = new System.Drawing.Size(94, 20);
            this.prozentEins.TabIndex = 6;
            this.prozentEins.TextChanged += new System.EventHandler(this.prozentEins_TextChanged);
            // 
            // prozentZwei
            // 
            this.prozentZwei.Location = new System.Drawing.Point(147, 136);
            this.prozentZwei.Name = "prozentZwei";
            this.prozentZwei.Size = new System.Drawing.Size(93, 20);
            this.prozentZwei.TabIndex = 7;
            this.prozentZwei.TextChanged += new System.EventHandler(this.prozentZwei_TextChanged);
            // 
            // prozentDrei
            // 
            this.prozentDrei.Location = new System.Drawing.Point(147, 162);
            this.prozentDrei.Name = "prozentDrei";
            this.prozentDrei.Size = new System.Drawing.Size(93, 20);
            this.prozentDrei.TabIndex = 8;
            this.prozentDrei.TextChanged += new System.EventHandler(this.prozentDrei_TextChanged);
            // 
            // prozentVier
            // 
            this.prozentVier.Location = new System.Drawing.Point(148, 188);
            this.prozentVier.Name = "prozentVier";
            this.prozentVier.Size = new System.Drawing.Size(93, 20);
            this.prozentVier.TabIndex = 9;
            this.prozentVier.TextChanged += new System.EventHandler(this.prozentVier_TextChanged);
            // 
            // prozentFünf
            // 
            this.prozentFünf.Location = new System.Drawing.Point(148, 214);
            this.prozentFünf.Name = "prozentFünf";
            this.prozentFünf.Size = new System.Drawing.Size(93, 20);
            this.prozentFünf.TabIndex = 10;
            this.prozentFünf.TextChanged += new System.EventHandler(this.prozentFünf_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(178, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Prozent:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Note:";
            // 
            // eins
            // 
            this.eins.Location = new System.Drawing.Point(24, 110);
            this.eins.Name = "eins";
            this.eins.Size = new System.Drawing.Size(100, 20);
            this.eins.TabIndex = 16;
            this.eins.TextChanged += new System.EventHandler(this.eins_TextChanged);
            // 
            // zwei
            // 
            this.zwei.Location = new System.Drawing.Point(24, 136);
            this.zwei.Name = "zwei";
            this.zwei.Size = new System.Drawing.Size(100, 20);
            this.zwei.TabIndex = 17;
            this.zwei.TextChanged += new System.EventHandler(this.zwei_TextChanged);
            // 
            // drei
            // 
            this.drei.Location = new System.Drawing.Point(24, 162);
            this.drei.Name = "drei";
            this.drei.Size = new System.Drawing.Size(100, 20);
            this.drei.TabIndex = 18;
            this.drei.TextChanged += new System.EventHandler(this.drei_TextChanged);
            // 
            // vier
            // 
            this.vier.Location = new System.Drawing.Point(24, 188);
            this.vier.Name = "vier";
            this.vier.Size = new System.Drawing.Size(100, 20);
            this.vier.TabIndex = 19;
            this.vier.TextChanged += new System.EventHandler(this.vier_TextChanged);
            // 
            // fuenf
            // 
            this.fuenf.Location = new System.Drawing.Point(24, 214);
            this.fuenf.Name = "fuenf";
            this.fuenf.Size = new System.Drawing.Size(100, 20);
            this.fuenf.TabIndex = 20;
            this.fuenf.TextChanged += new System.EventHandler(this.fuenf_TextChanged);
            // 
            // einstellungen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 316);
            this.Controls.Add(this.fuenf);
            this.Controls.Add(this.vier);
            this.Controls.Add(this.drei);
            this.Controls.Add(this.zwei);
            this.Controls.Add(this.eins);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.prozentFünf);
            this.Controls.Add(this.prozentVier);
            this.Controls.Add(this.prozentDrei);
            this.Controls.Add(this.prozentZwei);
            this.Controls.Add(this.prozentEins);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.modusv);
            this.Controls.Add(this.anzahlv);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Speichern);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "einstellungen";
            this.Text = "Einstellungen";
            this.Load += new System.EventHandler(this.einstellungen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.anzahlv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Speichern;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown anzahlv;
        private System.Windows.Forms.ComboBox modusv;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox prozentEins;
        private System.Windows.Forms.TextBox prozentZwei;
        private System.Windows.Forms.TextBox prozentDrei;
        private System.Windows.Forms.TextBox prozentVier;
        private System.Windows.Forms.TextBox prozentFünf;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox eins;
        private System.Windows.Forms.TextBox zwei;
        private System.Windows.Forms.TextBox drei;
        private System.Windows.Forms.TextBox vier;
        private System.Windows.Forms.TextBox fuenf;
    }
}