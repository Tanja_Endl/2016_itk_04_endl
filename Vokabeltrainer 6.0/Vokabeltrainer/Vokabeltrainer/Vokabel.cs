﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VokTrainer_1
{
   public class Vokabel
    {
        public string deutsch;
        public string englisch;
        public double rand = 0;

        public Vokabel(string deutsch, string englisch, double rand)
        {
            this.deutsch = deutsch;
            this.englisch = englisch;
            this.rand = rand;
        }

        public string Deutsch 
            { 
                get { return deutsch; } 
                set { deutsch = value; } 
            }

        public string Englisch
            {
                get{return englisch;}
                set{englisch = value;}
            }

        public double Rand
            {
                get { return rand; }
                set { rand = value; }
            }
    }
}
