﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VokTrainer_1
{
    class Optionen
    {

        string file = "optionen.txt";


        public int Anzahl { get; set; }
        public bool Modus { get; set; }

        public double einsPro { get; set; }
        public double zweiPro { get; set; }
        public double dreiPro { get; set; }
        public double vierPro { get; set; }
        public double fuenfPro { get; set; }

        public string eins { get; set; }
        public string zwei { get; set; }
        public string drei { get; set; }
        public string vier { get; set; }
        public string fuenf { get; set; }

        public bool isDirty;

        static Optionen opt = null;


        private Optionen()
        {
            ReadOptionen();
        }

        public static Optionen GetOptionen()
        {
            if (opt == null)
            {
                opt = new Optionen();

            }

            return opt;
        }


        public void ReadOptionen()
        {

            if (!File.Exists(file))
            {
                StreamWriter neueDatei = new StreamWriter(file);
                neueDatei.WriteLine("2;true");
                neueDatei.Flush();
                neueDatei.Close();
            }
            try
            {
                StreamReader sr = new StreamReader(file);
                string optionen = "";
                string benotung = "";
                string note = "";

                while (sr.Peek() != -1)
                {
                    string a = sr.ReadLine();
                    if (a.Contains(";"))
                    {
                        optionen = a;
                    }
                    else if (a.Contains(":"))
                    {
                        benotung = a;
                    }
                    else if (a.Contains(","))
                    {
                        note = a;
                    }

                }
                if (optionen == null || optionen == "")
                {
                    optionen = "2;true";
                }

                string[] split = optionen.Split(';');
                string[] spli = benotung.Split(':');
                string[] sp = note.Split(',');

                Anzahl = Convert.ToInt32(split[0]);
                Modus = Convert.ToBoolean(split[1]);

                einsPro = Convert.ToDouble(spli[0]);
                zweiPro = Convert.ToDouble(spli[1]);
                dreiPro = Convert.ToDouble(spli[2]);
                vierPro = Convert.ToDouble(spli[3]);
                fuenfPro = Convert.ToDouble(spli[4]);

                eins = sp[0];
                zwei = sp[1];
                drei = sp[2];
                vier = sp[3];
                fuenf = sp[4];

                sr.Close();
            }
            catch (Exception ex)
            {
                throw new VTException("Folgender Fehler ist ausgelöst worden:", ex);
            }
        }

        public void WriteOptionen()
        {
            StreamWriter sw = new StreamWriter(file);
            sw.WriteLine(Anzahl + ";" + Modus);
            sw.WriteLine(einsPro + ":" + zweiPro + ":" + dreiPro + ":" + vierPro + ":" + fuenfPro);
            sw.WriteLine(eins + "," + zwei + "," + drei + "," + vier + "," + fuenf);

            sw.Flush();
            sw.Close();
        }
    }
}
