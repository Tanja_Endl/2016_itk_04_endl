﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VokTrainer_1
{
    public class VTException:ApplicationException
    {

        public VTException():base()
        {}

        public VTException(string s):base(s)
        {}

        public VTException(string s, Exception e):base(s,e)
        {
            string file = "vt_errors.log";
            StreamWriter sw = new StreamWriter(file,true);
            sw.WriteLine(DateTime.Now+"-"+e);
            sw.Flush();
            sw.Close();

        }

    }
}
