﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VokTrainer_1
{
   public class Vokabeltrainer
    {
       public List<Vokabel> vokli = new List<Vokabel>();
        Optionen o = Optionen.GetOptionen();
        public List<bool> pruefe = new List<bool>();
        
    

       public List<Vokabel> ReadFromFile(string file)
        {
            try
            {
                StreamReader sr = new StreamReader(file);

                if (o.Modus == false)
                {

                    for (int i = 0; i < o.Anzahl & !sr.EndOfStream; i++)
                    {

                        string row = sr.ReadLine();
                        string[] split = row.Split(';');
                        vokli.Add(new Vokabel(split[0], split[1], 0));
                    }
                }
                else
                {
                    for (int i = 0; i < o.Anzahl && !sr.EndOfStream; i++)
                    {
                        string row = sr.ReadLine();
                        string[] split = row.Split(';');
                        vokli.Add(new Vokabel(split[1], split[0], 0));
                    }
                }

                sr.Close();
                return vokli;
            }
            catch (Exception ex)
            {
                throw new VTException("Folgender Fehler ist ausgelöst worden:", ex);
            }

            


        }

       public List<bool> CheckEntries(List<Eingabe> einli, ref string message)
       {
           int richtig = 0;
           double prozent;
           int anzahl = 0;
           
           foreach (Eingabe e in einli)
           {
               anzahl++;
              
            if (e.eingabe == e.ve.Englisch.ToUpper())
                {
                    
                    richtig++;
                    pruefe.Add(true);
                }
           else
                {
                    
                    pruefe.Add(false);
                }
            }
           prozent = 100 / (double)anzahl * (double)richtig;

           if (prozent >= o.einsPro)
           {
               message = "Erreichte Prozent " + prozent + "\n Note:" + o.eins;
           }
           else if (prozent >= o.zweiPro)
           {
               message= "Erreichte Prozent " + prozent + "\n Note:" + o.zwei;
           }
           else if (prozent >= o.dreiPro)
           {
               message= "Erreichte Prozent " + prozent + "\n Note:" + o.drei;
           }
           else if (prozent >= o.vierPro)
           {
               message= "Erreichte Prozent " + prozent + "\n Note:" + o.vier;
           }
           else
           {
               message= "Erreichte Prozent " + prozent + "\n Note:" + o.fuenf;
           }
           return pruefe;

       }

       public void SortEntries(List<Vokabel> vokli)
       {
           Random randy = new Random();


           for (int i = 0; i < vokli.Count; i++)
           {
               vokli[i].Rand = randy.Next(1, 10);
           }

           vokli.Sort(delegate(Vokabel a, Vokabel b)
           { return a.rand.CompareTo(b.rand); });

       }
    }
}
