﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VokTrainer_1
{
    public class Eingabe
    {
        
        public string eingabe { get; set; }
        public Vokabel ve { get; set; }

        public Eingabe(string eingabe, Vokabel ve)
        {
            this.eingabe = eingabe;
            this.ve = ve;
        }

    }
}