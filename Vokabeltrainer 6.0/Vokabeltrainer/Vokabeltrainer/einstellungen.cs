﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VokTrainer_1
{
    public partial class einstellungen : Form
    {
        Optionen o;
        public einstellungen()
        {
            InitializeComponent();
            o = Optionen.GetOptionen();
        }


        private void einstellungen_Load(object sender, EventArgs e)
        {
            o.ReadOptionen();

            anzahlv.Value = o.Anzahl;
            prozentEins.Text = Convert.ToString(o.einsPro);
            prozentZwei.Text = Convert.ToString(o.zweiPro);
            prozentDrei.Text = Convert.ToString(o.dreiPro);
            prozentVier.Text = Convert.ToString(o.vierPro);
            prozentFünf.Text = Convert.ToString(o.fuenfPro);
            eins.Text = o.eins;
            zwei.Text = o.zwei;
            drei.Text = o.drei;
            vier.Text = o.vier;
            fuenf.Text = o.fuenf;
           
            if (o.Modus == true)
            {
                modusv.SelectedItem = "Englisch -> Deutsch";
            }
            else
            {
                modusv.SelectedItem = "Deutsch -> Englisch";
            }

        }

        private void Speichern_Click(object sender, EventArgs e)
        {
            bool m;
            if (modusv.SelectedItem == "Englisch -> Deutsch")
            {
                m = true;
            }
            else
            {
                m = false;
            }

            int a = (int)anzahlv.Value;
            double pe = Convert.ToDouble(prozentEins.Text);
            double pz = Convert.ToDouble(prozentZwei.Text);
            double pd = Convert.ToDouble(prozentDrei.Text);
            double pv = Convert.ToDouble(prozentVier.Text);
            double pf = Convert.ToDouble(prozentFünf.Text);

            string ei = eins.Text;
            string zw = zwei.Text;
            string dr = drei.Text;
            string vi = vier.Text;
            string fu = fuenf.Text;

            if (o.isDirty == true)
            {
                o.Modus = m;
                o.Anzahl = a;
                o.einsPro = pe;
                o.zweiPro = pz;
                o.dreiPro = pd;
                o.vierPro = pv;
                o.fuenfPro = pf;
                o.eins = ei;
                o.zwei = zw;
                o.drei = dr;
                o.vier = vi;
                o.fuenf = fu;
                o.WriteOptionen();
            }

        }

        private void prozentEins_TextChanged(object sender, EventArgs e)
        {
           o.isDirty = true;
        }

        private void prozentZwei_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void prozentDrei_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void prozentVier_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void prozentFünf_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void eins_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void zwei_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void drei_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void vier_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void fuenf_TextChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void anzahlv_ValueChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }

        private void modusv_SelectedIndexChanged(object sender, EventArgs e)
        {
            o.isDirty = true;
        }




       
    }
}
