﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDelegateLift_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Lift l = new Lift("Lift", 500);
            l.zuschwer += l_zuschwer;
            l.Zusteigen(new Person() { Name = "Hauns", Gewicht = 40 });
            l.Zusteigen(new Person() { Name = "Franz", Gewicht = 100 });
            l.Zusteigen(new Person() { Name = "Jörg", Gewicht = 60 });
            l.Zusteigen(new Person() { Name = "Nadine", Gewicht = 50 });
            l.Zusteigen(new Person() { Name = "Oliver", Gewicht = 50 });
            l.Zusteigen(new Person() { Name = "Katja", Gewicht = 100 });
            l.Zusteigen(new Person() { Name = "Sebastian", Gewicht = 200 });



        }

        static void l_zuschwer(Person p)
        {
           
            Console.WriteLine(p.Name + " Bitte aussteigen!");
        }
    }
}
