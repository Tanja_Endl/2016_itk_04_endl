﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDelegateLift_1
{
    class Lift
    {
        public delegate void DelTyp(Person p);
        public event DelTyp zuschwer;

        public string Name { get; set; }
        public int MAXgewicht { get; set; }
        private List<Person> pli = new List<Person>();


        public Lift(string name, int max)
        {
            Name = name;
            MAXgewicht = max;
        }

        public void Zusteigen(Person p)
        {
            int pgewicht = 0;
            pli.Add(p);
            
            foreach (Person a in pli)
            {
                pgewicht += a.Gewicht;   
            }

            if (pgewicht > MAXgewicht)
            {
                int ideal, idealIndex;
                ideal = pgewicht - pli[0].Gewicht;
                idealIndex = 0;

                for (int i = 1; i < pli.Count; i++)
                {
                    int aktuellerKombination = pgewicht - pli[i].Gewicht;
                    if (aktuellerKombination > ideal)
                    {
                        ideal = aktuellerKombination;
                        idealIndex = i;
                    }
                }

                Person flieger = pli[idealIndex];
                pli.Remove(flieger);
                zuschwer(flieger);
            }
        }
    }
}
