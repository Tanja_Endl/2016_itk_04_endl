﻿/*------------------------------------------------------
 * Name: KategorieMenu.cs                               |
 * Autor: Tanja Endl                                    |
 * Datum: 18.05.2016                                    |
 * Beschreibung: Form von Kategorie auswahl und         |
 *                 schreiben/ändern/insert in DB        |
 * -----------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace VokTrainer_1
{
    public partial class KategorieMenu : Form
    {
        public KategorieMenu()
        {
            InitializeComponent();


            OleDbConnection con = Connection.GetConnection();
            FillKat();
        }

        OleDbConnection con = Connection.GetConnection();
        public Kat acKat;
        public int katid;

        public void FillKat()
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select ID, Bezeichnung from Kategorie order by Bezeichnung asc";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader.GetString(1);
                acKat = new Kat(id, name);
                listBoxKat.Items.Add(acKat);
            }
        }

        private void listBoxKat_SelectedIndexChanged(object sender, EventArgs e)
        {
            acKat = (Kat)listBoxKat.SelectedItem;
            textBox1.Clear();
            textBox1.Enabled = true;
            textBox1.Text = acKat.Name;
            buttonDelete.Enabled = true;
            buttonSave.Enabled = true;
            acKat.isNew = true;
            Kat kat = (Kat)listBoxKat.SelectedItem;
            string bez = kat.Name;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select ID, Bezeichnung from Kategorie order by Bezeichnung asc";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader.GetString(1);

                if (bez == name)
                {
                    katid = id;
                }
            }
        }

        public int KatId
        {
            get { return this.katid; }
        }

        public Button Button1
        {
            get { return this.button1; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            //buttonDelete.Enabled = true;
            buttonSave.Enabled = true;
            acKat.isNew = true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            acKat.Name = textBox1.Text;
            textBox1.Text.Replace("'", "\"");
            if (acKat.isNew == true)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                cmd.CommandText = "insert into Kategorie (Bezeichnung) values ('" + acKat.Name + "');";
                cmd.ExecuteNonQuery();
            }
            else if (acKat.isDirty == true)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                cmd.CommandText = "update Kategorie set Bezeichnung='" + acKat.Name + "' where ID=" + acKat.ID + ";";
                cmd.ExecuteNonQuery();
            }
            else
            {
                MessageBox.Show("Es wurde keine Kategorie verändert");
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;

            cmd.CommandText = "select count (*) from vokabel where kategorie = " + this.acKat.ID;
            if ((int)cmd.ExecuteScalar() > 0)
            {
                MessageBox.Show("Löschen nicht möglich da Vokabeln unter dieser Kategorie bestehen");
            }
            else
            {
                cmd.CommandText = "delete from Kategorie where ID =" + acKat.ID;
                cmd.ExecuteNonQuery();
            }
        }
    }
}