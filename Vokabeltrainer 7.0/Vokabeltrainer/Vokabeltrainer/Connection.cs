﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace VokTrainer_1
{
    public class Connection
    {
        static OleDbConnection con = null;
        static string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=vokabeltrainer1.mdb";


        public static OleDbConnection GetConnection()
        {
            if (con == null)
            {
                con = new OleDbConnection(conStr);
                con.Open();
            }

            return con;
         //   con.Close();
        }
    }
}
