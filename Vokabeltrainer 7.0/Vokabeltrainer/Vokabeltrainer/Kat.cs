﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VokTrainer_1
{
    public class Kat
    {
        private int id;
        private string name;

        public bool isDirty { get; set; }
        public bool isNew { get; set; }

        public int ID
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    isDirty = true;
                    id = value;
                }
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    isDirty = true;
                    name = value;
                }
            }
        }

        public Kat(int id, string name, bool isNew = false)
        {
            this.id = id;
            this.name = name;

            this.isDirty = false;
            this.isNew = isNew;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
