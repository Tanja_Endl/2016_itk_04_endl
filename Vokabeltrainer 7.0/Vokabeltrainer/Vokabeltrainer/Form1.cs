﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;

namespace VokTrainer_1
{
    public partial class Form1 : Form
    {
        Panel p = new Panel();
        List<Vokabel> vokli = new List<Vokabel>();
        Optionen o = Optionen.GetOptionen();
        Vokabeltrainer vt = new Vokabeltrainer();
        List<Eingabe> eingabe = new List<Eingabe>();
        List<bool> bli = new List<bool>();
        OleDbConnection con;
        KategorieMenu kate;
        int idKat;


        public Form1()
        {
            this.AutoSize = true;

            InitializeComponent();

            p.AutoSize = true;
            p.Name = "Panel";
            p.Left = 150;
            p.Top = 50;
            p.Height = 750;
            p.Width = 450;
            p.BackColor = Color.White;

            Button check = new Button();

            check.Size = new Size(40, 40);
            check.Location = new Point(35, 35);
            check.Text = "Prüfe";


            check.Top = 300;
            check.Left = 100;

            check.Click += check_Click;

            Button reload = new Button();

            reload.Size = new Size(50, 40);
            reload.Location = new Point(35, 35);
            reload.Text = "Reload";
            reload.Top = 350;
            reload.Left = 90;



            reload.Click += new System.EventHandler(this.delete_Click);

            this.Controls.Add(p);
            this.Controls.Add(check);
            this.Controls.Add(reload);
        }

        void check_Click(object sender, EventArgs e)
        {
            string message = "";
            CheckEntries();
            bli.Clear();
            bli = vt.CheckEntries(eingabe, ref message);
            ColorCheck(bli);
            MessageBox.Show(message);

        }

        private void delete_Click(object sender, EventArgs e)
        {
            DeletePanel(p);
        }

        //public List<Vokabel> ReadFromFile(string file)
        //{
        //    StreamReader sr = new StreamReader(file);

        //    if (o.Modus == false)
        //    {

        //        for (int i = 0; i < o.Anzahl & !sr.EndOfStream; i++)
        //        {

        //            string row = sr.ReadLine();
        //            string[] split = row.Split(';');
        //            vokli.Add(new Vokabel(split[0], split[1], 0));
        //        }
        //    }
        //    else
        //    {
        //        for (int i = 0; i < o.Anzahl && !sr.EndOfStream; i++)
        //        {
        //            string row = sr.ReadLine();
        //            string[] split = row.Split(';');
        //            vokli.Add(new Vokabel(split[1], split[0], 0));
        //        }
        //    }

        //    sr.Close();
        //    return vokli;

        //}

        public void CreateEntries(List<Vokabel> vokli, Panel p)
        {
            int height = 35;
            int width = 35;
            int count = 0;

            foreach (Vokabel a in vokli)
            {
                Label dLabel = new Label();
                TextBox eBox = new TextBox();
                dLabel.Text = a.Deutsch;
                dLabel.Name = a.Deutsch;
                dLabel.Tag = a.Deutsch;
                dLabel.Top = height + count;
                dLabel.Left = width;

                eBox.Name = a.Englisch;
                eBox.Tag = a.Englisch;
                eBox.Top = height + count;
                eBox.Left = width + 100;

                count += 35;

                p.Controls.Add(dLabel);
                p.Controls.Add(eBox);
            }
        }

        public void CheckEntries()
        {
            eingabe.Clear();
            foreach (Control c in Controls)
            {

                if (c.Name == "Panel")
                {
                    foreach (Control d in c.Controls)
                    {
                        Vokabel v = new Vokabel("deutsch", Convert.ToString(d.Tag), 0);

                        if (d is TextBox)
                        {
                            eingabe.Add(new Eingabe(d.Text.ToUpper(), v));


                        }
                    }
                }
            }
        }

        public void ColorCheck(List<bool> pruefe)
        {
            int i = 0;

            foreach (Control d in p.Controls)
            {
                if (d is TextBox)
                {

                    if (pruefe[i] == true)
                    {
                        d.BackColor = Color.Green;
                        i++;
                    }
                    else if (pruefe[i] == false)
                    {
                        d.BackColor = Color.Red;
                        i++;
                    }
                }
            }
        }

        //public void SortEntries(List<Vokabel> vokli)
        //{
        //    Random randy = new Random();


        //    for (int i = 0; i < vokli.Count; i++)
        //    {
        //        vokli[i].Rand = randy.Next(1, 10);
        //    }

        //    vokli.Sort(delegate(Vokabel a, Vokabel b)
        //    { return a.rand.CompareTo(b.rand); });

        //}

        public void DeletePanel(Panel p)
        {
            p.Controls.Clear();
            vt.SortEntries(vokli);
            CreateEntries(vokli, p);
        }

        private void öffnenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                if (DialogResult.Cancel != openFileDialog1.ShowDialog())
                {
                    this.vokli.Clear();
                    p.Controls.Clear();
                    string file = openFileDialog1.FileName;
                    //string sub = file.Substring(file.Length-4,4);

                    //if (sub==".voc")
                    //{
                    vt.ReadFromDB(idKat);
                    CreateEntries(vokli, p);
                    //}
                    //else
                    //{
                    //    throw new VTException("Falsches Dateiformat!");
                    //}                
                }
            }
            catch (Exception ex)
            {
                throw new VTException("Folgender Fehler ist ausgelöst worden:", ex);
            }
        }

        private void beendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (p.Controls.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Wirklich beenden?", "Wirklich?", MessageBoxButtons.OKCancel);
                if (dr == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
            else
            {
                Application.Exit();
            }

        }

        private void einstellungToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            einstellungen i = new einstellungen();

            DialogResult rs = i.ShowDialog();
            if (rs == DialogResult.OK)
            {

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.vokli.Clear();
            p.Controls.Clear();
            OleDbConnection con = Connection.GetConnection();
            KategorieMenu k = new KategorieMenu();
            this.kate = new KategorieMenu();
            this.idKat = kate.acKat.ID;

            List<Vokabel> vo;

            k.Button1.Click += delegate(object sender2, EventArgs e2)
            {
                this.vokli.Clear();
                Vokabeltrainer vt = new Vokabeltrainer();
                vo = vt.ReadFromDB(k.KatId);
                CreateEntries(vo, p);
            };

            k.ShowDialog();


        }

        private void kategorieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.vokli.Clear();
            p.Controls.Clear();
            OleDbConnection con = Connection.GetConnection();
            KategorieMenu k = new KategorieMenu();
            this.kate = new KategorieMenu();
            this.idKat = kate.acKat.ID;

            List<Vokabel> vo;

            k.Button1.Click += delegate(object sender2, EventArgs e2)
            {
                this.vokli.Clear();
                Vokabeltrainer vt = new Vokabeltrainer();
                vo = vt.ReadFromDB(k.KatId);
                CreateEntries(vo, p);
            };

            k.ShowDialog();
        }

    }
}
