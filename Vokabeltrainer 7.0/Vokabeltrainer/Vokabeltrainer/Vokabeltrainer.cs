﻿/*------------------------------------------------------
 * Name: Vokabeltrainer.cs                              |
 * Autor: Tanja Endl                                    |
 * Datum: 16.03.2016                                    |
 * Beschreibung: Logisches befüllen des Vakabeltrainers |
 * -----------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.OleDb;

namespace VokTrainer_1
{
    public class Vokabeltrainer
    {
        public List<Vokabel> vokli = new List<Vokabel>();
        Optionen o = Optionen.GetOptionen();
        public List<bool> pruefe = new List<bool>();
        OleDbConnection con = Connection.GetConnection();
        
        public List<Vokabel> ReadFromDB(int idKat)
        {
            try
            {
                if (o.Modus == false)
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "select de, en from Vokabel where Kategorie = " + idKat + ";";

                    OleDbDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        string de = reader.GetString(0);
                        string en = reader.GetString(1);
                        vokli.Add(new Vokabel(de, en, 0));
                    }


                }
                else
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "select de, en from Vokabel where Kategorie = " + idKat + ";";

                    OleDbDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        string de = reader.GetString(0);
                        string en = reader.GetString(1);
                        vokli.Add(new Vokabel(en, de, 0));
                    }
                }
                return vokli;
            }
            catch (Exception ex)
            {
                throw new VTException("Folgender Fehler ist ausgelöst worden:", ex);
            }
        }

        public List<bool> CheckEntries(List<Eingabe> einli, ref string message)
        {
            int richtig = 0;
            double prozent;
            int anzahl = 0;

            foreach (Eingabe e in einli)
            {
                anzahl++;

                if (e.eingabe == e.ve.Englisch.ToUpper())
                {

                    richtig++;
                    pruefe.Add(true);
                }
                else
                {

                    pruefe.Add(false);
                }
            }
            int falsch = anzahl - richtig;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "insert into Ergebnis values ('" + DateTime.Now + "'," + richtig + "," + falsch + ")";
            cmd.ExecuteNonQuery();
            prozent = 100 / (double)anzahl * (double)richtig;

            if (prozent >= o.einsPro)
            {
                message = "Erreichte Prozent " + prozent + "\n Note:" + o.eins;
            }
            else if (prozent >= o.zweiPro)
            {
                message = "Erreichte Prozent " + prozent + "\n Note:" + o.zwei;
            }
            else if (prozent >= o.dreiPro)
            {
                message = "Erreichte Prozent " + prozent + "\n Note:" + o.drei;
            }
            else if (prozent >= o.vierPro)
            {
                message = "Erreichte Prozent " + prozent + "\n Note:" + o.vier;
            }
            else
            {
                message = "Erreichte Prozent " + prozent + "\n Note:" + o.fuenf;
            }
            return pruefe;

        }

        public void SortEntries(List<Vokabel> vokli)
        {
            Random randy = new Random();


            for (int i = 0; i < vokli.Count; i++)
            {
                vokli[i].Rand = randy.Next(1, 10);
            }

            vokli.Sort(delegate(Vokabel a, Vokabel b)
            { return a.rand.CompareTo(b.rand); });

        }
    }
}
