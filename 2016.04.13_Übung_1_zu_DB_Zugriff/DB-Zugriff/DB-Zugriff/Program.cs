﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace DB_Zugriff
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb";

            OleDbConnection con = new OleDbConnection(conStr);    //!!!nur einmal öffnen und schließen!!!
            try
            {

            
            con.Open();

            Console.WriteLine("Suche nach Buch per ISBN:");
            string isbn = Console.ReadLine();
            //string isbn = "3-021169-41-0";
            Console.WriteLine("Ihr gewähltes Buch: ");
            getByISBN(con, isbn);
            getCheapestBook(con);

            con.Close();
            }
            catch (OleDbException oledbEx)
            {

                Console.WriteLine("Etwas ist mit der Datenbankverbindung fehlgeschlagen!"); ;
            }
        }


        static void getByISBN(OleDbConnection con, string isbn)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select Titel ,Autor from Buch where ISBN='" + isbn + "'";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string t = reader.GetString(0);
                string a = reader.GetString(1);
                Console.WriteLine(t + " - " + a);
            }
            reader.Close();
        }

        static void getCheapestBook(OleDbConnection con)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select isbn, titel, autor, preis, erscheinungsjahr from Buch where preis=(select min(preis) from buch)"; //evt.: order by und erste Zeile ausgeben
            OleDbDataReader reader = cmd.ExecuteReader();
           
            while (reader.Read())
            {
                string isbn = reader.GetString(0);
                string titel = reader.GetString(1);
                string autor = reader.GetString(2);
                int preis = reader.GetInt32(3);
                int datum = reader.GetInt32(4);

                Console.WriteLine("Billigstes Buch:");
                Console.WriteLine(isbn + "-" + titel + "-" + autor + "-" + preis + "-" + datum);
            }

            reader.Close();
        }
    }
}
