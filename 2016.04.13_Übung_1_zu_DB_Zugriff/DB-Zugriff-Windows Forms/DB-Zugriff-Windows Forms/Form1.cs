﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace DB_Zugriff_Windows_Forms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void speichern_Click(object sender, EventArgs e)
        {
            string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb";
            OleDbConnection con = new OleDbConnection(conStr);
            try
            {
                //!!!nur einmal öffnen und schließen!!!
                con.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;

                int themen = Convert.ToInt32(themenid.Text);
                string visbn = isbn.Text;
                string vtitel = titel.Text;
                string vautor = autor.Text;
                int vpreis = Convert.ToInt32(preis.Text);
                int vdatum = Convert.ToInt32(erscheinungsjahr.Text);

                cmd.CommandText = "insert into Buch(ThemenID, ISBN, Titel, Autor, Preis, Erscheinungsjahr) values (" + themen + ",'" + visbn + "','" + vtitel + "','" + vautor + "'," + vpreis + "," + vdatum + ");";
                cmd.ExecuteNonQuery();
            }
            catch (OleDbException oledbEx)
            {
                MessageBox.Show(oledbEx + "Etwas ist mit der Verbindung schiefgelaufen!");
            }
            finally
            {

                con.Close();
                MessageBox.Show("Ihr Buch wurde in der Datenbank gespeichert!");
            }
        }
    }
}
