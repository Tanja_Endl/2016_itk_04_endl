﻿namespace DB_Zugriff_Windows_Forms
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.themenid = new System.Windows.Forms.TextBox();
            this.isbn = new System.Windows.Forms.TextBox();
            this.titel = new System.Windows.Forms.TextBox();
            this.autor = new System.Windows.Forms.TextBox();
            this.preis = new System.Windows.Forms.TextBox();
            this.erscheinungsjahr = new System.Windows.Forms.TextBox();
            this.speichern = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Themen-ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ISBN:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(82, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Autor:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Titel:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(82, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Preis:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Erscheinungsjahr:";
            // 
            // themenid
            // 
            this.themenid.Location = new System.Drawing.Point(153, 35);
            this.themenid.Name = "themenid";
            this.themenid.Size = new System.Drawing.Size(100, 20);
            this.themenid.TabIndex = 6;
            // 
            // isbn
            // 
            this.isbn.Location = new System.Drawing.Point(153, 63);
            this.isbn.Name = "isbn";
            this.isbn.Size = new System.Drawing.Size(100, 20);
            this.isbn.TabIndex = 7;
            // 
            // titel
            // 
            this.titel.Location = new System.Drawing.Point(153, 94);
            this.titel.Name = "titel";
            this.titel.Size = new System.Drawing.Size(100, 20);
            this.titel.TabIndex = 8;
            // 
            // autor
            // 
            this.autor.Location = new System.Drawing.Point(153, 120);
            this.autor.Name = "autor";
            this.autor.Size = new System.Drawing.Size(100, 20);
            this.autor.TabIndex = 9;
            // 
            // preis
            // 
            this.preis.Location = new System.Drawing.Point(153, 146);
            this.preis.Name = "preis";
            this.preis.Size = new System.Drawing.Size(100, 20);
            this.preis.TabIndex = 10;
            // 
            // erscheinungsjahr
            // 
            this.erscheinungsjahr.Location = new System.Drawing.Point(153, 172);
            this.erscheinungsjahr.Name = "erscheinungsjahr";
            this.erscheinungsjahr.Size = new System.Drawing.Size(100, 20);
            this.erscheinungsjahr.TabIndex = 11;
            // 
            // speichern
            // 
            this.speichern.Location = new System.Drawing.Point(106, 211);
            this.speichern.Name = "speichern";
            this.speichern.Size = new System.Drawing.Size(75, 23);
            this.speichern.TabIndex = 12;
            this.speichern.Text = "Speichern";
            this.speichern.UseVisualStyleBackColor = true;
            this.speichern.Click += new System.EventHandler(this.speichern_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.speichern);
            this.Controls.Add(this.erscheinungsjahr);
            this.Controls.Add(this.preis);
            this.Controls.Add(this.autor);
            this.Controls.Add(this.titel);
            this.Controls.Add(this.isbn);
            this.Controls.Add(this.themenid);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Buch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox themenid;
        private System.Windows.Forms.TextBox isbn;
        private System.Windows.Forms.TextBox titel;
        private System.Windows.Forms.TextBox autor;
        private System.Windows.Forms.TextBox preis;
        private System.Windows.Forms.TextBox erscheinungsjahr;
        private System.Windows.Forms.Button speichern;
    }
}

