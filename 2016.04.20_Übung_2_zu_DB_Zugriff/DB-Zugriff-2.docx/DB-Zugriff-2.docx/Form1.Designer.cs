﻿namespace DB_Zugriff_2.docx
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxThemenID = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxBuch = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.BoxDatum = new System.Windows.Forms.TextBox();
            this.BoxAutor = new System.Windows.Forms.TextBox();
            this.BoxTitel = new System.Windows.Forms.TextBox();
            this.BoxISBN = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxThemenID
            // 
            this.listBoxThemenID.FormattingEnabled = true;
            this.listBoxThemenID.Location = new System.Drawing.Point(38, 53);
            this.listBoxThemenID.Name = "listBoxThemenID";
            this.listBoxThemenID.Size = new System.Drawing.Size(112, 147);
            this.listBoxThemenID.TabIndex = 0;
            this.listBoxThemenID.SelectedIndexChanged += new System.EventHandler(this.listBoxThemenID_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "ThemenID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(206, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Buch:";
            // 
            // listBoxBuch
            // 
            this.listBoxBuch.FormattingEnabled = true;
            this.listBoxBuch.Location = new System.Drawing.Point(209, 53);
            this.listBoxBuch.Name = "listBoxBuch";
            this.listBoxBuch.Size = new System.Drawing.Size(112, 147);
            this.listBoxBuch.TabIndex = 3;
            this.listBoxBuch.SelectedIndexChanged += new System.EventHandler(this.listBoxBuch_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.BoxDatum);
            this.panel1.Controls.Add(this.BoxAutor);
            this.panel1.Controls.Add(this.BoxTitel);
            this.panel1.Controls.Add(this.BoxISBN);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(327, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(235, 263);
            this.panel1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(68, 176);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Ändern";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BoxDatum
            // 
            this.BoxDatum.Location = new System.Drawing.Point(116, 110);
            this.BoxDatum.Name = "BoxDatum";
            this.BoxDatum.Size = new System.Drawing.Size(100, 20);
            this.BoxDatum.TabIndex = 11;
            // 
            // BoxAutor
            // 
            this.BoxAutor.Location = new System.Drawing.Point(116, 74);
            this.BoxAutor.Name = "BoxAutor";
            this.BoxAutor.Size = new System.Drawing.Size(100, 20);
            this.BoxAutor.TabIndex = 9;
            // 
            // BoxTitel
            // 
            this.BoxTitel.Location = new System.Drawing.Point(116, 38);
            this.BoxTitel.Name = "BoxTitel";
            this.BoxTitel.Size = new System.Drawing.Size(100, 20);
            this.BoxTitel.TabIndex = 8;
            // 
            // BoxISBN
            // 
            this.BoxISBN.Location = new System.Drawing.Point(116, 3);
            this.BoxISBN.Name = "BoxISBN";
            this.BoxISBN.Size = new System.Drawing.Size(100, 20);
            this.BoxISBN.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Erscheinungsjahr:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Autor:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Titel:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "ISBN:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 354);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listBoxBuch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxThemenID);
            this.Name = "Form1";
            this.Text = "Buchänderung";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxThemenID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxBuch;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox BoxDatum;
        private System.Windows.Forms.TextBox BoxAutor;
        private System.Windows.Forms.TextBox BoxTitel;
        private System.Windows.Forms.TextBox BoxISBN;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}

