﻿/* ----------------------------------------------------
 * Klasse:Form1.cs
 * Autor: Tanja Endl
 * Datum:20.04.2016
 * Beschreibung: User wählt ThemenName und Buch aus.
 *               Kann das Buch daraufhin verändern.
 *-----------------------------------------------------
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace DB_Zugriff_2.docx
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        OleDbConnection con;

        private void Form1_Load(object sender, EventArgs e)
        {
            string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=htblbiblio.mdb";
            try
            {
                con = new OleDbConnection(conStr);
                con.Open();
                ThemenListeBefuellen();
            }
            catch (OleDbException oledb)
            {
                MessageBox.Show(oledb+"Etwas ist mit der Verbindung schiefgelaufen!");
            }
            


        }

        public void ThemenListeBefuellen()
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from Themengruppe order by Bezeichnung asc";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader.GetString(1);
                ThemenID themen = new ThemenID(id, name);

                listBoxThemenID.Items.Add(themen);
            }
        }

        public void ThemenWahl()
        {
            ThemenID wahl = (ThemenID)listBoxThemenID.SelectedItem;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select BuchNr, ThemenID, ISBN, Titel, Autor, Preis, Erscheinungsjahr from Buch order by Titel";
            OleDbDataReader reader = cmd.ExecuteReader();
            listBoxBuch.Items.Clear();
            while (reader.Read())
            {
                Buch buch = new Buch(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetInt32(5), reader.GetInt32(6));
                if (buch.ThemenID == wahl.vThemenID)
                {

                    listBoxBuch.Items.Add(buch);
                }

            }
        }

        public void BuchWahl()
        {

            Buch wahl = (Buch)listBoxBuch.SelectedItem;
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select BuchNr, ThemenID, ISBN, Titel, Autor, Preis, Erscheinungsjahr from Buch WHERE Titel = '" + wahl.Titel + "'";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int buchNr = reader.GetInt32(0);
                string isbn = reader.GetString(2);
                string titel = reader.GetString(3);
                string autor = reader.GetString(4);
                int datum = reader.GetInt32(6);


                BoxISBN.Text = isbn;
                BoxTitel.Text = titel;
                BoxAutor.Text = autor;
                BoxDatum.Text = Convert.ToString(datum);
            }

        }

        public void BuchAendern()
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;

            Buch b = (Buch)listBoxBuch.SelectedItem;


            if (b.Isbn != BoxISBN.Text)
            {
                cmd.CommandText = "update buch set ISBN = '" + BoxISBN.Text + "' where BuchNr ="+b.BuchNr;
            }
            if (b.Titel != BoxTitel.Text)
            {
                cmd.CommandText = "update buch set Titel = '" + BoxTitel.Text + "' where BuchNr =" + b.BuchNr;
            }
            if (b.Autor != BoxAutor.Text)
            {
                cmd.CommandText = "update buch set Autor = '" + BoxAutor.Text + "' where BuchNr =" + b.BuchNr;
            }
            if (Convert.ToString(b.Datum) != BoxDatum.Text)
            {
                cmd.CommandText = "update buch set Erscheinungsjahr = '" + Convert.ToInt32(BoxDatum.Text) + "' where BuchNr =" + b.BuchNr;
            }
            try
            {
                MessageBox.Show(cmd.ExecuteNonQuery() + " Row affected.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Es wurde nichts verändert!");
            }
           
        }

        private void listBoxThemenID_SelectedIndexChanged(object sender, EventArgs e)
        {
            ThemenWahl();

        }

        private void listBoxBuch_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuchWahl();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.Close();
        }

      

        private void button1_Click(object sender, EventArgs e)
        {
            BuchAendern();
        }


    }
}
