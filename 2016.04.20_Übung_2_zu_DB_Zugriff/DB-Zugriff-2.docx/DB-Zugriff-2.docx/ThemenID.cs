﻿/* --------------------------------------------------
 * Klasse: ThemenID.cs
 * Autor: Tanja Endl
 * Datum: 20.04.2016
 * Beschreibung: Speichert die ThemenID
 * --------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_Zugriff_2.docx
{
    class ThemenID
    {  
        public int vThemenID { get; set; }
        public string ThemenName { get; set; }

        public ThemenID(int themenID, string themenName)
        {
            vThemenID = themenID;
            ThemenName = themenName;
        }
        public override string ToString()
        {
            return ThemenName;
        }
  }
}
