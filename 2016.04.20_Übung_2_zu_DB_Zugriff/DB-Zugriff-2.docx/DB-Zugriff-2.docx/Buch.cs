﻿/*--------------------------------------------------
 * Klasse:Buch.cs
 * Autor:Tanja Endl
 * Datum:20.04.2016
 * Beschreibung: Speichert die daten von einem Buch
 * --------------------------------------------------
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_Zugriff_2.docx
{
    class Buch
    {
        public int BuchNr { get; set; }
        public int ThemenID { get; set; }
        public string Isbn { get; set; }
        public string Titel { get; set; }
        public string Autor { get; set; }
        public int Preis { get; set; }
        public int Datum { get; set; }

        public Buch(int buchnr,int themenID, string isbn, string titel, string autor, int preis, int datum)
        {
            BuchNr = buchnr;
            ThemenID = themenID;
            Isbn = isbn;
            Titel = titel;
            Autor = autor;
            Preis = preis;
            Datum = datum;
        }
        public override string ToString()
        {
            return Titel;
        }

    }
}
